package com.techuniversity.biblioteca;

import com.techuniversity.biblioteca.components.JWTBuilder;
import com.techuniversity.biblioteca.controllers.LibroController;
import com.techuniversity.biblioteca.model.AutorModel;
import com.techuniversity.biblioteca.model.LibroModel;
import com.techuniversity.biblioteca.utils.EstadosLibro;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.bson.Document;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.TestMethodOrder;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class LibroControllerTest {

    @Test
    void contextLoads() {
    }

    private String crearUrlConPuerto(String url) {
        return "http://localhost:" + port + url;
    }

    @Autowired
    LibroController libroController;

    @Autowired
    JWTBuilder jwtBuilder;

    private String getToken() {
        return jwtBuilder.generarToken("Tester", "admin");
    }

    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();


    @Test
    public void testFindAll() {
        String token = getToken();

        ResponseEntity<List<LibroModel>> listLibroModel = libroController.getAllLibros(token, "0");
        assertTrue(listLibroModel.getBody().size() > 0);
    }


// GET DISPONIBLES RESPONSE ENTITY
// GET PRESTADOS RESPONSE ENTITY



    @Disabled
    @Test
    public void testGetHttpLibro() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = testRestTemplate.exchange(
                crearUrlConPuerto("/libros/TestBook"),
                HttpMethod.GET, entity, String.class
        );
        System.out.println(crearUrlConPuerto("/libros/TestBook"));
        String expected = "{\"id\":\"TestBook\",\"titulo\":\"Cien años de soledad\",\"autor\":{\"id\":\"null\",\"nombre\":\"Gabriel García Márquez\",\"nacionalidad\":\"ES\"},\"fecha_publi\":\"1967\",\"idioma\":\"ES\",\"editorial\":\"Planeta\",\"estado\":\"DISPONIBLE\"}";
        System.out.println(response.getBody());
        JSONAssert.assertEquals(expected, response.getBody(), false);

    }


}
    /* Agregar producto test */

    /***
     {
     "id": "TestBook",
     "titulo":"Cien años de soledad",
     "autor": {
     "id" : "null",
     "nombre":"Gabriel García Márquez",
     "nacionalidad":"ES"},
     "fecha_publi": "1967",
     "idioma":"ES",
     "editorial": "Planeta",
     "estado":"DISPONIBLE"
     }
     ***/

