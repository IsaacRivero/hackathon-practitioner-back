package com.techuniversity.biblioteca.controllers;

import com.techuniversity.biblioteca.storage.FileSystemSotrageService;
import com.techuniversity.biblioteca.storage.StorageFileNotFoundException;
import com.techuniversity.biblioteca.storage.StorageProperties;
import com.techuniversity.biblioteca.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(path = "/portadas")
public class FileUploadController {

    private StorageService getStorageService() {
        return  new FileSystemSotrageService(new StorageProperties());
    }
    // Esta funcion devuelve una lista de url de los archivos.
    @GetMapping("/")
    public String listUploadedFiles(Model model) throws IOException {
        Stream<Path> paths = getStorageService().loadAll();
        Stream<String> links = paths.map(
                path -> {
                    UriComponentsBuilder ucb = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveFile",
                            path.getFileName().toString());
                    return ucb.build().toUri().toString();
                }
        );
        // Tranforma el canal en una lista de String
        List<String> lstLinks = links.collect(Collectors.toList());
        model.addAttribute("files", lstLinks);
        return "uploadLibros";

    }

    @GetMapping(value = "/files/{filename:.+}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImage(@PathVariable String filename) throws IOException {

        Resource file = getStorageService().loadAsResource(filename);
        InputStream is = file.getInputStream();
        byte [] imagen =  new byte[is.available()];
        is.read(imagen);
        return imagen;
    }

    //filename:.+ significa que no importa el tipo solo me quedo con el nombre
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = getStorageService().loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "arrachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }
    //metodo para subir los archivos
    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file")MultipartFile file, RedirectAttributes redirectAttributes) {
        //Subira el archivo
        getStorageService().store(file);
        //Añadirea un mensaje
        redirectAttributes.addFlashAttribute("message", "Has subido correctamente el archivo: " + file.getOriginalFilename());
        //redireccionar a la funcion get para sacar el listado.
        return "redirect:/portadas/";
    }

    // Siempre devuelve el mismo mensaje not found
    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handlerStorageFileNotFound(StorageFileNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
