package com.techuniversity.biblioteca.controllers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.techuniversity.biblioteca" + ""))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }
    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "Hackathon - Rest APi de listado de Libros y Autores", "Listado de libros de una Biblioteca", "Versión 1.0", "https://Techuniversity.com/terminos",
                new Contact("Lidia C.E., Isaac R.V., Javier P.A., Miguel Ángel M.L.", "https://www.biblioteca.com", "biblioteca@bbvaitspain.com"),"LICENCIA", "HTTPS://www.biblioteca.com/licencia", Collections.emptyList()
        );
    }
}