package com.techuniversity.biblioteca.components;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.security.auth.message.AuthException;

@Component
public class JWTBuilder {

    @Value("${jwt.issuer}")
    private String jwtIssuer;
    @Value("${jwt.secret}")
    private String jwtSecret;
    @Value("${jwt.expiry}")
    private Float jwtExpiry;

    RsaJsonWebKey rsaJsonWebKey;

    public JWTBuilder() {
    }

    public  String generarToken(String userId, String roles){
        try {
            JwtClaims jwtClaims = new JwtClaims();
            jwtClaims.setIssuer(jwtIssuer);
            jwtClaims.setExpirationTimeMinutesInTheFuture(jwtExpiry);
            jwtClaims.setAudience("ALL");
            jwtClaims.setStringListClaim("groups", roles);
            jwtClaims.setGeneratedJwtId();
            jwtClaims.setIssuedAtToNow();
            jwtClaims.setSubject("AUTHTOKEN");
            jwtClaims.setClaim("userID", userId);

            JsonWebSignature jsonWebSignature = new JsonWebSignature();

            jsonWebSignature.setPayload(jwtClaims.toJson());
            jsonWebSignature.setKey(rsaJsonWebKey.getRsaPrivateKey());
            jsonWebSignature.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
            jsonWebSignature.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

            return jsonWebSignature.getCompactSerialization();

        } catch (JoseException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public JwtClaims generarParseToken(String token) throws Exception {
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
        .setRequireExpirationTime()
        .setSkipSignatureVerification()
        .setAllowedClockSkewInSeconds(60)
        .setRequireSubject()
        .setExpectedIssuer(jwtIssuer)
        .setExpectedAudience("ALL")
        .setExpectedSubject("AUTHTOKEN")
        .setVerificationKey(rsaJsonWebKey.getKey())
        .setJwsAlgorithmConstraints(new AlgorithmConstraints(
                AlgorithmConstraints.ConstraintType.WHITELIST,
                AlgorithmIdentifiers.RSA_USING_SHA256
        )).build();
        try {
            JwtClaims jwtClaims = jwtConsumer.processToClaims(token);
            return jwtClaims;

        }catch (InvalidJwtException ex) {
            try {
                if (ex.hasExpired()) {
                    throw new Exception("Sesión Caducada por tiempo, en: " +
                            ex.getJwtContext().getJwtClaims().getExpirationTime());
                }
                if ((ex.hasErrorCode(ErrorCodes.AUDIENCE_INVALID))) {
                    throw new Exception("Audiencia incorrecta: " +
                            ex.getJwtContext().getJwtClaims().getAudience());
                }
                throw new AuthException((ex.getMessage()));

            } catch (MalformedClaimException me) {
                throw new AuthException("Mal formada clave");
            }
        }
    }

    public String validarToken (String token) throws Exception {
        String userId= null;
        JwtClaims jwtClaims = generarParseToken(token);
        userId = jwtClaims.getClaimValue("userID").toString();
        return userId;
    }

    @PostConstruct
    public void init(){
        try {
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            rsaJsonWebKey.setKeyId(jwtSecret);

        }catch (JoseException ex) {
            ex.printStackTrace();
        }
    }

}
