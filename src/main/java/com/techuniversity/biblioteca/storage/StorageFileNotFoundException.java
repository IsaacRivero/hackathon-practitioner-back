package com.techuniversity.biblioteca.storage;

public class StorageFileNotFoundException extends StorageException{

    public StorageFileNotFoundException(String mensaje) {
        super(mensaje);
    }

    public StorageFileNotFoundException(String mensaje, Throwable causa) {
        super(mensaje,causa);
    }
}
