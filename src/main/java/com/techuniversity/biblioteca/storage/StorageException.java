package com.techuniversity.biblioteca.storage;

public class StorageException  extends RuntimeException{

    public StorageException(String mensaje) {
        super(mensaje);
    }

    public StorageException(String mensaje, Throwable causa) {
        super(mensaje,causa);
    }
}
