package com.techuniversity.biblioteca.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {
    // Se limpia la carpeta de imagenes cada vez que se arranque la aplicación
    void init();
    // metodo para guardar un imagen/archivo , vale para cualquier tipo de archivo
    void store(MultipartFile file);
    //devolvera una lita de los imagenes/archivos guardados
    Stream<Path> loadAll();
    //devuelve la ruta de una imagen/archivo en concreto
    Path load(String filename);
    //devuelve la imagen/archivo como recurso
    Resource loadAsResource(String filename);
    //funciona que se encarga de borrar los datos.
    void deleteAll();

}
