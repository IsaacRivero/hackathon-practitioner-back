package com.techuniversity.biblioteca.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

public class FileSystemSotrageService implements StorageService{

    private final Path rootLocation;

    @Autowired
    public FileSystemSotrageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.init();
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException ex) {
            throw new StorageException("No se puede crear el directorio", ex);
        }
    }

    @Override
    public void store(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Archivo no valido, el archivo esta vacio");
            }
            Path destinationFile = this.rootLocation.resolve(Paths.get(file.getOriginalFilename()))
                    .normalize().toAbsolutePath();
            if (!destinationFile.getParent().equals(this.rootLocation.toAbsolutePath())) {
                throw new StorageException("No se puede almnacenar en el directorio definido");
            }
            try (InputStream stream = file.getInputStream()) {
                Files.copy(stream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException ex) {
            throw new StorageException("No se puede guardar el archivo", ex);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            // recupera todos los archivos de la ruta
            return Files.walk(this.rootLocation,1)
                    .filter(path -> !path.equals(this.rootLocation))
                    //aplica atributos relativize (saca toda la tura)
                    .map(this.rootLocation::relativize);
        } catch (IOException ex) {
            throw new StorageException("No se puede acceder al disco/directorio", ex);
        }
    }

    @Override
    public Path load(String filename) {
        return this.rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file =load(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return  resource;
            } else  {
                throw new StorageException("El archivo no existe: " + filename);
            }
        } catch (MalformedURLException ex) {
            throw new StorageException("La ruta del archivo es incorrecta", ex);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(this.rootLocation.toFile());
    }

}
