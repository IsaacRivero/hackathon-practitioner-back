package com.techuniversity.biblioteca.repository;

import com.techuniversity.biblioteca.model.LibroModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibroDAO extends MongoRepository<LibroModel, String> {

}
